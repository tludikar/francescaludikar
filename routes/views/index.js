var keystone = require('keystone'),
	React = require('react/addons');

require('node-jsx').install();


exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals,
		GalleryComp = React.createFactory(require("../../src/js/gallery/gallery.jsx"));
	
	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';

	view.on('init', function(next) {
		
		var galleryQuery = keystone.list('Image').model.find().where('homepage','true').sort('sortOrder').populate('project');
		
		galleryQuery.exec(function(err, result) {
			locals.gallery = result;

			if(req.params.image){
				var heroQuery = keystone.list('Image').model.findOne({
					slug: req.params.image
				}).populate('project');

				heroQuery.exec(function(err, result) {
					locals.image = result;
					locals.html = React.renderToString(GalleryComp({ images: locals.gallery, lightboxImage: locals.image }));
					next(err);
				});
			} else {
				locals.html = React.renderToString(GalleryComp({ images: locals.gallery, lightboxImage: locals.image }));
				next(err);
			}
		});
		
	});

	view.render('index');
	
};
