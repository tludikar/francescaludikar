var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals,
		Project = keystone.list('Project');

	
	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'about';

		// Load the current post
	view.on('init', function(next) {
		
		var q = keystone.list('Image').model.findOne({
			slug: req.params.image
		}).populate('project');
		
		q.exec(function(err, result) {
			locals.image = result;
			next(err);
		});
		
	});

	// Render the view
	view.render('partials/lightbox', {layout: false});
	
};
