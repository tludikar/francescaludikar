var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'contact';

	// view.query('gallery', keystone.list('Gallery').model.find().where('name','Homepage').sort('sortOrder'));
	
	// Render the view
	view.render('contact');
	
};
