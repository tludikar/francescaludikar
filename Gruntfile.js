'use strict()';

var config= {
	port: 3000
};

module.exports = function(grunt) {

	// Load grunt tasks automatically
	require('load-grunt-tasks')(grunt);

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		express: {
			options: {
				port: config.port
			},
			dev: {
				options: {
					script: 'keystone.js',
					debug: true
				}
			}
		},

		bump: {
			options: {
				pushTo: 'production'
			}
		},

		uglify: {
			compile: {
				files: [{
					expand: true,
					cwd: 'public/js',
					src: '**/*.js',
					dest: 'public/js'
				}]
			}
		},

		browserify: {
			dev: {
				options: {
					transform: [ 'browserify-shim', 'reactify'],
					browserifyOptions: {
						standalone: 'name'
					}
				},
				files: {
					'public/js/app.js':['src/js/app.js']
				}
			}
		},

		bower : {
			install : {
				options : {
					targetDir : 'src/js/vendor',
					layout : 'byComponent',
					verbose: true,
					cleanup: true
				}
			}
		},

		copy: {
			dev: {
				expand: true,
				cwd: 'src/js',
				src: ['vendor/requirejs/require.js'],
				dest: 'public/js/',
				flatten: false,
				filter: 'isFile'
			}
		},

		requirejs: {
			compile: {
				options: {
					name: 'app',
					baseUrl: 'src/js/',
					mainConfigFile: 'src/js/app.js',
					out: 'public/js/app.js',
					optimize: 'none',
					optimizer: 'none',
					onBuildWrite: function (moduleName, path, contents) {
						return contents.replace(/jsx!/g, '');
					}
				}
			}
		},

		compass: {
            options: {
                sassDir: 'src/styles',
                cssDir: 'public/styles',
                imagesDir: 'src/images',
                javascriptsDir: 'src/scripts',
                fontsDir: 'src/styles/fonts',
                importPath: 'src/library',
                relativeAssets: true
            },
            dist: {},
            server: {
                options: {
                    debugInfo: true
                }
            }
        },

		jshint: {
			options: {
				reporter: require('jshint-stylish'),
				force: true
			},
			all: [ 'routes/**/*.js',
				 	'models/**/*.js'
			],
			server: [
				'./keystone.js'
			]
		},

		concurrent: {
			dev: {
				tasks: ['nodemon', 'node-inspector', 'watch'],
				options: {
					logConcurrentOutput: true
				}
			}
		},

		'node-inspector': {
			custom: {
				options: {
					'web-host': 'localhost'
				}
			}
		},

		nodemon: {
			debug: {
				script: 'keystone.js',
				options: {
					nodeArgs: ['--debug'],
					env: {
						port: config.port
					},
					watch: ['models/**', 'routes/**']
				}
			}
		},

		watch: {
			js: {
				files: [
					'model/**/*.js',
					'routes/**/*.js',
				],
				tasks: ['jshint:all']
			},
			express: {
				files: [
					'keystone.js'
				],
				tasks: ['jshint:server', 'concurrent:dev']
			},
			compass: {
                files: ['src/styles/{,*/}*.{scss,sass}'],
                tasks: ['compass']
            },
            handlebars: {
            	files: ['src/templates/**/*.hbs'],
            	tasks: ['handlebars']
            },
            javascript: {
            	files: ['src/js/**/*.js', 'src/js/**/*.jsx'],
            	tasks: ['browserify']
            },
			livereload: {
				files: [
					'public/js/**/*.js',
					'public/styles/**/*.css',
					'public/styles/**/*.less',
					'templates/**/*.hbs',
					'node_modules/keystone/templates/**/*.jade'
				],
				options: {
					livereload: true
				}
			}
		}
	});

	// load jshint
	grunt.registerTask('lint', function(target) {
		grunt.task.run([
			'jshint'
		]);
	});

	// default option to connect server
	grunt.registerTask('serve', function(target) {
		grunt.task.run([
			'jshint',
			'concurrent:dev'
		]);
	});

	grunt.registerTask('server', function () {
		grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
		grunt.task.run(['serve:' + target]);
	});

};
