"use strict";
var $ = require('jquery');

(function(factory){
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	} else if (typeof module === 'object' && module.exports) {
		module.exports = factory(require('jquery'));
	} else {
		factory(jQuery);
	}
}(function($){

	var settings = {
			dataAttribute: "image",
			preload: false
		},
		images = {
			length: 0
		},
		dfd;

	var methods = {
		init: function(options) {
			dfd = $.Deferred();
			$.extend(settings, options);
			methods.start.apply(this);
			return dfd.promise();
		},
		start: function() {
			$.each(this, function(i, image){
				var imageTag = $(image).data(settings.dataAttribute),
					$image = $(image);

				images[imageTag] = new Image();
				images[imageTag].src = imageTag;
				images.length++;

				images[imageTag].onload = function(){
					if(!settings.preload){
						$image.attr('src', images[imageTag].src);
						$image.addClass("loaded");
					}
					delete images[imageTag];
					images.length--;
					inProgress(images);
 				};
			});
		},
		progress: function() {},
		finished: function() {}
	};

	var inProgress = function(images){
		if(images.length){
			dfd.notify(images.length);
			methods.progress.apply(this);
		} else {
			finished();
		}
	}

	var finished = function(){
		methods.finished.apply(this);
		dfd.resolve();
	}

	$.fn.lazyload = function(methodOrOptions){
		if ( methods[methodOrOptions] ) {
			return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.lazyload' );
		}
	};
	
}));