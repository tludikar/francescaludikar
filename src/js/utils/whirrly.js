var $ = require('jQuery'),
	Spinner = require("../vendor/spin.js/spin");

(function(factory){
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	} else if (typeof module === 'object' && module.exports) {
		module.exports = factory(require('jquery'));
	} else {
		factory(jQuery);
	}
}(function($){

	var settings = {
		color: "#FFF"
	};

	var _spin;

	var methods = {
		init: function(options) {
			$.extend(settings, options);
			_spin = new Spinner(settings).spin();
			methods.start.apply(this);
		},
		start: function() {
			this.html(_spin.el);
		},
		stop: function(callback) {
			this.empty();
			callback();
		},
		fadeOut: function(callback) {
			this.fadeOut(function(){
				if(typeof callback == 'function') callback();
			});
		},
		fadeIn: function(callback) {
			this.fadeIn(function(){
				if(typeof callback == 'function') callback();
			});
		}
	};

	$.fn.whirrly = function(methodOrOptions){
		if ( methods[methodOrOptions] ) {
			return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.whirrly' );
		}
	};
}));