"use strict";

var $ = require("jquery"),
	React = require("react"),
	router = require("./services/router"),
	Gallery = require("./gallery/gallery.jsx");

require("./utils/whirrly");

$('#loading').whirrly();

router.createRoute("home", "/", function(){
	React.render(<Gallery images={images}/>, document.getElementById('gallery'));
});

router.createRoute("image", "/{project}/{id}", function(project, id){
	React.render(<Gallery images={images} lightboxImage={heroImage}/>, document.getElementById('gallery'));
});

router.init();
