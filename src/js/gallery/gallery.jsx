/** @jsx React.DOM */

"use strict";

var React = require("react"),
	_ = require("underscore"),
	Masonry = (typeof window !== 'undefined') ? require("masonry-layout") : null,
	TileImages = require("./tile.jsx"),
	Lightbox = require("./lightbox.jsx");

(typeof window !== 'undefined') ? require("../utils/lazyload") : null;

var Gallery = React.createClass({
		getInitialState: function(){
			var currentIndex = -1;

			if(this.props.lightboxImage){
				currentIndex = _.findIndex(this.props.images, { _id: this.props.lightboxImage._id });
			}

			return ({
				lightboxImage: this.props.lightboxImage || null,
				currentIndex: currentIndex
			});
		},
		sortTiles: function(){
			this._msnry = new Masonry("#gallery", {
				"isFitWidth": true,
				"itemSelector": ".item",
				"gutter": 10,
				"visibleStyle": { opacity: 1, transform: "scale(1)" }
			});
		},
		openLightbox: function(image){
			this.setState({
				lightboxImage: image,
				currentIndex: _.findIndex(this.props.images, { _id: image._id })
			});
		},
		closeLightbox: function(){
			this.setState({
				lightboxImage: null,
				currentIndex: -1
			});
		},
		nextImage: function(){
			var newIndex = (this.state.currentIndex + 1 >= this.props.images.length) ? 0 : this.state.currentIndex + 1,
				image = this.props.images[newIndex];

			this.setState({
				lightboxImage: image,
				currentIndex: newIndex
			});
		},
		prevImage: function(){
			var newIndex = (this.state.currentIndex - 1 < 0) ? this.props.images.length - 1 : this.state.currentIndex - 1,
				image = this.props.images[newIndex];
			
			this.setState({
				lightboxImage: image,
				currentIndex: newIndex
			});
		},
		componentDidMount: function(){
			$("img.lazyload").lazyload().then(function(){
				$('#loading').whirrly('fadeOut', function(){
					$('.item').animate({opacity: 1}, 1000);
				});
			}).then(function(){
				$("img.lazyload").lazyload({
					dataAttribute: "fullimage",
					preload: true
				});
			}).then(this.sortTiles);

			$(window).on('resize', this.sortTiles);
		},
		render: function(){
			return (
				<div>
					<Lightbox heroImage={this.state.lightboxImage} closeLightbox={this.closeLightbox} onNextImage={this.nextImage} onPrevImage={this.prevImage} />
					<div id="gallery">
						{this.props.images.map(function(image) {
							return <TileImages key={image._id} image={image} openLightbox={this.openLightbox} />;
						}.bind(this))}
					</div>
				</div>
			);
		}
	});

module.exports = Gallery;