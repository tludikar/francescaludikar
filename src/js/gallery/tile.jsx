"use strict";

var React = require("react"),
	router = (typeof window !== 'undefined') ? require("../services/router") : null;

var TileImages = React.createClass({
	clickHandler: function(e){
		e.preventDefault();
		router.setUrl(this.props.image.post_url);
		this.props.openLightbox(this.props.image);
	},
	render: function(){
		return (
			<a href={this.props.image.post_url} className='item' onClick={this.clickHandler}>
				<img src="" data-image={this.props.image.thumbnail_url} alt={this.props.image.name} data-fullimage={this.props.image.fullimage_url} className="lazyload" />
				<div className="rollover">
					<span>{this.props.image.project.name}</span>
				</div>
			</a>
		);
	}
});

module.exports = TileImages;