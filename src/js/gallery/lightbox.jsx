"use strict";

var React = require("react"),
	router = (typeof window !== 'undefined') ? require("../services/router") : null;

var Lightbox = React.createClass({
	getInitialState: function() {
		return {
			show: this.props.heroImage ? true : false,
			caption: false
		};
	},
	componentDidMount: function(){
		$('#loading').whirrly('fadeIn');
		$(window).on("keydown", function(e){
			e.preventDefault();
			if(e.keyCode == 37){
				this.props.onPrevImage();
			}
			else if(e.keyCode == 39){
				this.props.onNextImage();
			}
			else if(e.keyCode == 27){
				this.closeLightbox();
			}
		}.bind(this));
	},
	componentWillUnmount: function(){
		$(window).off("keydown", function(){});
	},
	closeLightbox: function(){
		this.setState({
			show: false,
			caption: false
		});
		this.props.closeLightbox();
	},
	toggleCaption: function(){
		var imageWidth = $("#imageview img").width();
		$(".overlay").width(imageWidth + 1);
		this.setState({ caption: !this.state.caption });
	},
	shouldComponentUpdate: function(nextProps, nextState){
		if(nextProps.heroImage){
			router.setUrl(nextProps.heroImage.post_url);
		} else {
			router.setUrl("/");
		}
		return true;
	},
	onImageLoad: function(){
		var $displayImage = $(React.findDOMNode(this.refs.displayImage)),
			$infoBar = $(React.findDOMNode(this.refs.infoBar)),
			$overlay = $(React.findDOMNode(this.refs.overlay)),
			imageWidth = $displayImage.outerWidth(),
			imageHeight = $displayImage.outerHeight();

		$infoBar.width(imageWidth - 20).height(imageHeight - 20);
		$infoBar.width(imageWidth - 20).height(imageHeight - 20);
		$overlay.css("margin-bottom", window.getComputedStyle($displayImage.get(0)).marginBottom);

		$("#loading").whirrly("fadeOut", function(){
			$displayImage.css("opacity", 1);
		});
	},
	render: function(){
		if(!this.props.heroImage) return(<div id="lightbox" className="hide"></div>);

		var cx = React.addons.classSet,
			captionSet = cx({
				"height-animation": true,
				"changed": this.state.caption
			});
		return (
			<div id="lightbox">
				<span className="icon-close close" onClick={this.closeLightbox}></span>
				<div id="wrapper">
					<div id="imageview" className="center image">
						<img ref="displayImage" className="display-image" src={this.props.heroImage.fullimage_url} onLoad={this.onImageLoad}/>
						<div ref="infoBar" id="info-bar" className="opacity-animation">
							<ul>
								<li id="information" onClick={this.toggleCaption}><i className="icon-info"></i></li>
								<li id="share"><i className="icon-share"></i>
									<ul>
										<li><a href={"https://twitter.com/home?status=http://francescaludikar.ca" + this.props.heroImage.post_url} target="_blank"><i className="icon-twitter"></i></a></li>
										<li><a href={"https://www.facebook.com/sharer/sharer.php?u=http://francescaludikar.ca" + this.props.heroImage.post_url} target="_blank"><i className="icon-facebook"></i></a></li>
										<li><a href={"https://plus.google.com/share?url=http://francescaludikar.ca" + this.props.heroImage.post_url} target="_blank"><i className="icon-google-plus"></i></a></li>
										<li><a href={"http://www.tumblr.com/share?v=3&u=http://francescaludikar.ca/" + this.props.heroImage.post_url} target="_blank"><i className="icon-tumblr"></i></a></li>
									</ul>
								</li>
							</ul>
						</div>

						<div ref="overlay" className="overlay">
							<div id="image_info" className={captionSet}>
								<div>
									<p>Client: <span>{this.props.heroImage.client}</span></p>
									<p>Project: <span>{this.props.heroImage.project.name}</span></p>
								</div>
							</div>
						</div>
					</div>

					<div className="left arrow icon-arrow flip" onClick={this.props.onPrevImage}></div>
					<div className="right arrow icon-arrow" onClick={this.props.onNextImage} ></div>
				</div>
			</div>
		);
	}
});

module.exports = Lightbox;