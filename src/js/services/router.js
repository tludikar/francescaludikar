"use strict";

var crossroads = require("crossroads");
require("history");

var instance;

var Router = function(){

	var routes = {};

	if(instance){
		return instance;
	}
	instance = this;

	History.Adapter.bind(window, 'popstate', function(){
		crossroads.parse(window.location.pathname);
	});

	this.createRoute = function(title, url, callback){
		routes[title] = crossroads.addRoute(url);
		routes[title].matched.add(function(){
			callback.apply(this, arguments);
		});
	};
	
	this.setUrl = function(url, data){
		data = data || {};
		history.pushState(data, "", url);
		crossroads.parse(url);
	};

	this.init = function(){
		crossroads.parse(window.location.pathname);
	};

	return this;

};

module.exports = new Router;