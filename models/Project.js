var keystone = require('keystone'),
	Types = keystone.Field.Types;

/**
 * PostCategory Model
 * ==================
 */

var Project = new keystone.List('Project', {
	autokey: { from: 'name', path: 'key', unique: true }
});

Project.add({
	name: { type: String, required: true }
});

Project.relationship({ ref: 'Image', path: 'categories' });

Project.register();
