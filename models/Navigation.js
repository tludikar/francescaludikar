var keystone = require('keystone'),
	Types = keystone.Field.Types;

/**
 * PostCategory Model
 * ==================
 */

var NavLinks = new keystone.List('NavLinks', {
	sortable: true,
	map: { name: 'label' },
	label: "Navigation"
});

NavLinks.add({
	label: { type: String },
	link: { type: Types.Url },
	state: { type: Types.Select, options: 'active, disabled', default: 'disabled' },
});

NavLinks.defaultColumns = 'label, link, state';

NavLinks.register();