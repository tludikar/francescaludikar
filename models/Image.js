var keystone = require('keystone'),
	Types = keystone.Field.Types,
	cloudinary = require('cloudinary');

/**
 * Gallery Model
 * =============
 */

var Images = new keystone.List('Image', {
	autokey: { from: 'name', path: 'slug', unique: true }
});

Images.add({
	name: { type: String, required: true },
	client: { type: String },
	homepage: { type: Boolean },
	publishedDate: { type: Date, default: Date.now },
	project: { type: Types.Relationship, ref: 'Project', many: false },
	image: { type: Types.CloudinaryImage }
});

Images.schema.virtual("thumbnail_url").get(function(){
	return cloudinary.url(this.image.public_id, { width: 300, height: 500, crop: "limit" });
});

Images.schema.virtual("fullimage_url").get(function(){
	return cloudinary.url(this.image.public_id, { width: 800, height: 1000, crop: "limit" });
});

Images.schema.set('toJSON', {
	transform: function (image, rtn, options) {
		rtn.thumbnail_url = image.thumbnail_url;
		rtn.fullimage_url = image.fullimage_url;
		rtn.post_url = "/" + image.project.key + "/" + image.slug;
	}
});

Images.register();
